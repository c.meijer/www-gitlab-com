---
layout: handbook-page-toc
title: "ABM Campaign Tactic Processes"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Account Based Marketing Campaign Tactics
The Account Based Strategy team uses various tactics when launching an Account Based Marketing (ABM) campaign. This page will host the steps needed to take by an ABM Manager to successfully launch those tactics. 

### Buyer Progression Webcasts 
This is a GitLab-hosted virtual event with Webcast type configuration in Zoom. This is the preferred setup for larger GitLab-hosted virtual events (up to 1,000 attendees) that requires registration due to the integration with Marketo for automated lead flow and event tracking. GitLab-hosted webcast type is a single room virtual event that allows multiple hosts. Attendees cannot share audio/video unless manually grated access. 

### ABM & Verticurl  
The ABS team will be working with the [Verticurl](/handbook/marketing/demand-generation/campaigns/agency-verticurl/) team to execute email marketing setup and other tasks in Marketo. We will use issues and an issue board to remain aligned. The triage process will be fairly straightforward with the ABM Manager opening and completing all necessary tasks within issues and handing off to the final copy to the Verticurl team.

##### Labels and Issue board

- [**ABM and Verticurl Issue board**](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/boards/2543577)

| Label | Use Case | 
| ------------ | ---------- | 
| ABM-Verticurl::blocked | ABM Manager adds when the issue does not have enough information for Verticurl to work on the issue or the asks are not complete. ABM Manager is to address then re-add the ABM-Verticurl::triage label to flow back through the process. | 
| ABM-Verticurl::wip | ABM Manager adds when the issue is 100% cleaned up and they are ready to send the work to Verticurl to complete | 
| ABM-Verticurl::review | Verticurl adds when they are ready for ABM manager to review the work they have completed | 

##### Triage process
1. ABM Manager: Issues are submitted with final copy and all details included. When details are confirmed and final, ABM Manager will move to ABM-Verticurl::wip and assign to ABM Verticurl DRI - Karthikeyan K.
1. Verticurl: Verticurl DRI adds email to the Email Marketing Google Calendar for the anticipated send date.
     - The requested send date is the due date of the issue. If the requested send date is less than 5 Business Days from the date it is passed to Karthikeyan K, the date of the issue will be scheduled 5 business days out to abide by SLAs.
1. Verticurl: Verticurl DRI completes the setup in Marketo, then sends a test email to the ABM Manager who created the issue
     - SLA: 24 hours (from day assigned to Verticurl team DRI to provide)
1. Verticurl: Verticurl DRI comments into the issue mentioning the ABM Manager and documenting that the test email was sent to their inbox.
1. Verticurl: Verticurl DRI moves status to ABM-Verticurl::review
1. Verticurl: Verticurl DRI posts a screenshot of the email in the issue description
     - This chrome extension is recommended for screenshots: GoFullPage
1. GitLab: Issue requester must review and approve email (or provide corrections) via comment in the issue
     - SLA: 24 hours from when the test email is sent and comment added to issue). Feedback and approval in a timeley manner is critical on the GitLab side.
1. Verticurl: Verticurl DRI makes any necessary corrections. If no corrections needed and approval provided by reviewer, Verticurl DRI sets the email to send (time for send to be determined in issue comments).
1. Verticurl: Verticurl checks that email was sent, confirms in comments (tagging issue requester) and closes out the issue.

#### Buyer Progression Webcast SLAs

Please note that all Buyer Progression Webcasts are considered webcasts with new content, which means they have a **-45 business day** SLA requirement. Please open this epic and confirm your speaker and date based on the epic task list prior to moving forward with other tasks. All other SLAs for ABM Buyer Progression Webcast can be found [here] (https://docs.google.com/spreadsheets/d/1otehEQ1_LPnRs0ilX4CZB4j4qJwBx2t-3R6bZjMTI2Y/edit?usp=sharing)

#### Ownership of Buyer Progression Webcast Tasks

* Project Management in GitLab (epic, issue, timeline creation): ABM Manager
* GitLab-Hosted Webcast Calendar: ABM Manager
* Zoom setup and integration: ABM Manager
* Landing page and email copy (templated with limited customization, if any): ABM Manager
* Marketo program, SFDC campaign: ABM Manager
* Landing page, email creation: Verticurl
* Host dry run and run day-of event Zoom meeting: ABM Manager
* Day of event moderator: PMM/TMM/ABM Manager
     * The on-screen MC of the webcast (welcomes attendees, introduces presenters, reads questions from audience to the presenter to answer)

#### Buyer Progression Webcast Epic Code

```
> Naming convention: [Webcast Title] - [3-letter Month] [Webcast Date], [Year]

## Need to know
### Team
* [ ] ABM Manager: 
* [ ] Presenter:
* [ ] Q&A Coordinator:
* [ ] Q&A Support:
* [ ] Host:
### Date and Name
(don’t populate until officially confirmed)
* [ ] Dry run date and time: `MM-DD-YYYY`
* [ ] Webcast date and time: `example - 10:00am PDT`
* [ ] Official webcast name: `webcast title`
* [ ] Topic: `topic`

## Content and resources
*  [Landing Page]  `to be added when live`
* Landing Page and Invite Copy 
* [Dry Run & Day of Agenda] 
* [Salesforce Campaign]
* [Marketo Program]()
* Campaign UTM - (Format: campaign tag - change to all lowercase, no spaces, hyphens, underscores, or special characters)

## Webcast date and speaker checklist 

NOTE: Please make sure your Webcast date and speaker are confirmed before starting the process for any of the other sections. Moving a Webcast date is extremely time-intensive once setup is complete.

* [ ] Check the [webcast gcal](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV8xcXZlNmc4MWRwOTFyOWhldnRrZmQ5cjA5OEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t) and determine a date that is at least ~45 business days out. Best practice is to host a webcast on a Wednesday or Thursday. Ensure the suggested date does not over-saturate the calendar.
* [ ] Open PMM/TMM [speaker request issue](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/new?issuable_template=pmm-speaker-request) with your proposed date and topic.
* [ ] Once your speaker is confirmed, ensure the date is still available and add to [Webcast Google calendar](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV8xcXZlNmc4MWRwOTFyOWhldnRrZmQ5cjA5OEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t) when final date is agreed upon - Remember to block the calendar for an additional 30 mins longer before and after the slotted time. Naming Convention for Calendar - [WC Hosted] Name of Webcast and Start-End Time/Time Zone of event (example - 9:00am - 12:00pm PST)
* [ ] Add speaker(s) and team to calendar invite and topic to the description.
* [ ] Uncheck the calendar settings `Modify Event` and `Invite Others` under Guest Permissions so invitees are not able to modify the event or add additional guests.
* [ ] Schedule dry run ~5 days before event and add to [Webcast Google calendar](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV8xcXZlNmc4MWRwOTFyOWhldnRrZmQ5cjA5OEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t)dry run with the same information as well as this epic linked. Only schedule for 60 minutes. Naming Convention for Calendar - [WC Dry Run] Name of Webcast Start-End Time/Time Zone of dry run (example - 9:00am - 10:00am PST)


### Webcast Preparation Checklist
ABM Manager to create, assign as needed and link below:

* [ ] [Webcast Prep Issue](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new?issuable_template=webcast_prep) 
* [ ] [Dry Run Prep and Agenda] (https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new?issuable_template=webcast_dry_run)
* [ ] [Program Tracking](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new?issuable_template=webcast_program_tracking) 
* [ ] [Write Copy Issue](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new?issuable_template=webcast_write_copy)
* [ ] [Target list creation Issue](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new?issuable_template=webcast_target_list_creation)
* [ ] [Marketo Landing Page & Automation Issue](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new?issuable_template=webcast_request_mkto_landing_page)
* [ ] [Sales Nominated Issue](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new?issuable_template=webcast_request_sales_nominated)
* [ ] [Email Invitation Issue](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new?issuable_template=webcast_request_email_invite)

Please Note: We do not send Marketo reminder emails for webcasts as Zoom sends a reminder the day before the webcast and an hour prior to the webcast.

* [ ] [Follow Up Email issue](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new?issuable_template=webcast_request_email_follow_up)
* [ ] [Add to Nurture](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new?issuable_template=webcast_request_add_nurture)
* [ ] [List Clean and Upload](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=event-clean-upload-list) - Zoom integration automatically uploads the registrants and their attendance status to SFDC but any notes, Q&A, etc. need to be added via this upload issue  - FMC creates, assigns to FMM and MOps
* [ ] [ABM Demandbase Campaign issue](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new?issuable_template=ABM_Demandbase_Campaign)
* [ ] [ABM Campaign PathFactory Track](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new?issuable_template=ABM_Campaign_PathFactory_Track)

/label ~"Webcast - GitLab Hosted" ~”Buyer Progression Webcast” ~"mktg-status::wip" ~"ABM Campaign"
```

#### Buyer Progression Webcast Setup
1. Step 1: [Configure Zoom](/handbook/marketing/revenue-marketing/field-marketing/workshop-webcast-how-to/#step-1-configure-zoom) - DRI: ABM Manager
1. Step 2: [Set up the webcast in Marketo/SFDC and integrate to Zoom](/handbook/marketing/revenue-marketing/field-marketing/workshop-webcast-how-to/#step-2-set-up-the-webcast-in-marketosfdc-and-integrate-to-zoom) - DRI: ABM Manager
1. Step 3A: [Update Marketo Tokens](/handbook/marketing/revenue-marketing/field-marketing/workshop-webcast-how-to/#step-3a-update-marketo-tokens) - DRI: ABM Manager and Verticurl
1. Step 3B: [Turn on Smart Campaigns in Marketo](/handbook/marketing/revenue-marketing/field-marketing/workshop-webcast-how-to/#step-3b-turn-on-smart-campaigns-in-marketo) - DRI: ABM Manager
1. Step 3C: [Create the Landing Page](/handbook/marketing/revenue-marketing/field-marketing/workshop-webcast-how-to/#step-3c-create-the-landing-page) - DRI: Verticurl
1. Step 4: [Webcast Invitations](/handbook/marketing/revenue-marketing/field-marketing/workshop-webcast-how-to/#step-4-webcast-invitations) - DRI:
Verticurl
1. Step 5: [Test Your Setup](/handbook/marketing/revenue-marketing/field-marketing/workshop-webcast-how-to/#step-5-test-your-setup) - DRI: ABM Manager

#### Post LIVE Webcast
1. [Converting the Webcast to an On-Demand Gated Asset](/handbook/marketing/revenue-marketing/field-marketing/workshop-webcast-how-to/#converting-the-webcast-to-an-on-demand-gated-asset) - DRI: ABM Manager
1. [Test your follow up emails and set to send](/handbook/marketing/revenue-marketing/field-marketing/workshop-webcast-how-to/#test-your-follow-up-emails-and-set-to-send) - DRI: Verticurl

